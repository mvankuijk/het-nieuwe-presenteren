$(document).ready(function(){
	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		
	 	if($("#main-content").hasClass("registreren")){
	 		$("form").hide();
	 		$(".mobile-message").show();	
	 	}
	 	
	}
	else{
		$("form").show();
		$(".mobile-message").hide();
	}
	
	
	
	var selectedTab,
		currentPage = window.location.pathname.split("/").slice(-1)[0],
		currentHash = window.location.href.split("#").slice(-1)[0];
		bedanktLocatie = $(".registreren form").attr("action"),
		template = $("tr#first"),
		addButton = $("body").find("input[name=add]").length;
		
	$("input[name=delete]").hide();	
		
			
	$('.registreren form').formExtender();
	$('.registreren form').validate({
		submitHandler : function(form) {
			$(window).trigger('change', {defaultinput: {disabled: true}});
			if (navigator.userAgent.indexOf("Firefox")!=-1){
				window.location.href = bedanktLocatie;
				window.print();
			}
			else{
				window.print();
				window.location.href = bedanktLocatie;
			}
		}
	});
	
	if(currentHash === "back"){
		$(".tab").hide();
		$("#promotie").show();
		$("ul.nav-tabs li").removeClass("current");
		$("a[data-target=promotie]").parent().addClass("current");
	}
		
	$(".nav-tabs a").click(function(e){
		selectedTab = $(this).attr("data-target");
		
		if(selectedTab != null){
			$(".rounded-tabs").removeClass("current");
			$(this).parent().addClass("current");
			$(".tab").hide();
			$("#" + selectedTab).fadeIn(500);
		}
		
	});
	
	
	if(addButton >= 1){
		
		$("input[name=meerschermen]").click(function(){
			var id = $(this).attr("id");
			$(".products").hide();
			
			if(id === "ja"){
				$(".multiple").show();
			}
			else{
				$(".single").show();			
			}
		});
		
		$("input[name=add]").click(function(){
		
			var newRow = template.clone().removeAttr("id"),
				numberOfRows = $("tbody tr").length + 1;
				
			newRow.children().find("select[name=extra-modelcode]").attr("name","extra-modelcode" + numberOfRows);
			newRow.children().find("input[name=extra-serienummer]").attr("name","extra-serienummer" + numberOfRows).val("");
			newRow.children().find(".number").text(numberOfRows + ".");
				
			newRow.appendTo($("table.multiple-input tbody"));
			
			if(numberOfRows === 10){
				$("input[name=add]").hide();
			}
			if(numberOfRows >= 2){
				$("input[name=delete]").show();
			}
			
		});
		
		$("input[name=delete]").click(function(){
			var numberOfRows = $("tbody tr").length;
			
			if(numberOfRows <= 10){
				$("tr:eq(" + numberOfRows + ")").remove();
				$("input[name=delete]").show();
				$("input[name=add]").show();
			}
			if(numberOfRows === 2){
				$("input[name=delete]").hide();
				$("input[name=add]").show();
			}
		});
		
		
	}
	
	
	$(function () {
		
	  $.scrollUp({
	    scrollName: 'scrollUp', // Element ID
	    topDistance: '300', // Distance from top before showing element (px)
	    topSpeed: 300, // Speed back to top (ms)
	    animation: 'fade', // Fade, slide, none
	    animationInSpeed: 200, // Animation in speed (ms)
	    animationOutSpeed: 200, // Animation out speed (ms)
	    activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	    scrollImg: { active: true, type: 'background', src: '/images/top.png' }
	  });
	  
	});
	
	$("#video").load("video.html");
	
	$(".btn_movie_view").click(function(e){
		e.preventDefault();
		loadPopup();
	});
	
	function loadPopup(){
		$("#video").bPopup({
            contentContainer:'.container',
            loadUrl: 'video.html',
            position :['auto','auto'],
            
            onClose: function(){
				$("#video").empty();
				$("#video").html("<a href='#' class='b-close'><img src='images/btn_movie_close.gif' alt='multimedia content close'></a><iframe width='640' height='480' src='//www.youtube.com/embed/riLsh8bikXs?rel=0&autohide=1&showinfo=0' frameborder='0' allowfullscreen></iframe>')");
			}
    	});
   	}
});
